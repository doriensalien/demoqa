package be.dorien.demoqa;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class PracticeForm {

    WebDriver chromeDriver = new ChromeDriver();

    @Test
    public void fillRequiredFields(){
        System.setProperty("webdriver.chrome.driver", "DemoQA\\chromedriver.exe");
        chromeDriver.navigate().to("https://demoqa.com/automation-practice-form");
        WebElement firstName = chromeDriver.findElement(By.id("firstName"));
        firstName.sendKeys("Dorien");
        WebElement lastName = chromeDriver.findElement(By.id("lastName"));
        lastName.sendKeys("Saliën");
        WebElement email = chromeDriver.findElement(By.id("userEmail"));
        email.sendKeys("dorien.salien@bignited.be");
        WebElement gender = chromeDriver.findElement(By.xpath("//label[@for='gender-radio-2']"));
        gender.click();
        WebElement mobileNumber = chromeDriver.findElement(By.id("userNumber"));
        mobileNumber.sendKeys("0123456789");
        WebElement submitButton = chromeDriver.findElement(By.id("submit"));
        submitButton.submit();
//        Check if pop up is visible
        Assert.assertTrue(chromeDriver.findElement(By.className("modal-content")).isDisplayed());
        WebElement studentName = chromeDriver.findElement(By.xpath("//td[text()='Student Name']/following-sibling::td"));
        Assert.assertEquals("Dorien Saliën", studentName.getText());
        WebElement studentEmail = chromeDriver.findElement(By.xpath("//td[text()='Student Email']/following-sibling::td"));
        Assert.assertEquals("dorien.salien@bignited.be", studentEmail.getText());
        WebElement studentGender = chromeDriver.findElement(By.xpath("//td[text()='Gender']/following-sibling::td"));
        Assert.assertEquals("Female", studentGender.getText());
        WebElement studentMobile = chromeDriver.findElement(By.xpath("//td[text()='Mobile']/following-sibling::td"));
        Assert.assertEquals("0123456789", studentMobile.getText());
    }
}
